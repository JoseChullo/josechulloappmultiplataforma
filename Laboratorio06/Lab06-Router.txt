----------------------------Laboratorio 06-------------------------
--------------------------React-navigation-------------------------
Nombre:Chullo Valeriano Jose Carlos
Grupo: A C15;
-------------------------------------------------------------------
|#1|-Configuracion del proyecto.

	*Se inicio con la creacion del laboratorio react-native lab06*
	Nuestra aplicaci�n debe correr sin problemas y mostrar un Hola Mundo.
	 Procederemos a crear una vista adiciona	
	Imagen N�1 => '1-4.bmp'
	Imagen N�2 => '1-8.gif'
	Imagen N�3 => "1-9.png"
	Imagen N�4 => "1-10.png"

|#2|-Refactorizar nuestro codigo.
	
	Ahora que vemos el funcionamiento correcto de nuestro navegador,
 	procederemos a refactorizar (ordenar) nuestro c�digo.
	Crearemos la carpeta src, donde crearemos la carpeta screens con los archivos Home.js y Details.js

	Imagen N�5 => "2-4.png"
	Imagen N�6 => "3-5.gif"

|#3|-Configuracion por defecto.
	
	Nosotros podemos definir opcionales adicionales en el segundo objeto
	pasado a nuestro navegador. Procederemos a configurar algunas opciones.

|#4| Parametros en rutas.

	Como durante el desarrollo de nuestras aplicaciones, tendremos vistas din�micas,
	ser�a muy �til pasar par�metros a las vistas para indicarles c�mo comportarse.
	Modificaremos Details.js para que le pase un par�metro de nombre a Profile.js 	

		

OBSERVACIONES

	* Se utiliza una nueva herramienta Vysor y la toma screenGif.
	* Para utilizar el componente reci�n creado, debemos importarlo e invocarlo como se vio en el laboratorio no es necesario que pongamos su extencion.
	* Como son etiquetas podemos a�adirles propiedades externas que le indican como comportarse.
	* Podemos modificar el estado con la funcion this.setState.
	* Como podemos observar estamos trabajando con los cambios de vistas de la app.